Hello team HTML24,

that is my final version of your chalange. If you would like to see more of my projects please check my:

portfolio:
[https://w-gluza.github.io/portfolio](https://w-gluza.github.io/portfolio)

or straight to GitHub
[https://github.com/w-gluza](https://github.com/w-gluza)

Please let me know what do you think,
Wioletta

# Brown Rabbit

This is the job test if you wish to be a web developer at HTML24

## Contents

When you have completed this test you will have demonstrated skills/knowledge in

- git
- html
- css
- js/jQuery
- Bootstrap

## Requirements

Apply responsivness, make sure that the page look good on the following media queries:

- Mobile - 375px
- Tablet - 768px
- Desktop - 1200px

Search functionality:

- You should be able to search through the content of the page and highlight the result

Slider:

- Start the slider with a random image every time the page is refreshed

Read more button:

- Use a click event to show the rest of the text

## Bonus Requirement

Pagination :

- You should be able to navigate through the articles. Make sure you have at least 15 articles.
- Use at least 1 loop and 1 click event

## Instructions

To complete this test.

1. Fork this repository
2. Create a website based on the supplied PSD-file
3. Create a pull request to this repository
