const dataSlides = [
  {
    key: '1a',
    src: './img/slider/slide__01.jpg',
    alt: 'Coffe Picture 1',
    figcaption: '0 FIRTS Description of the picture goes here'
  },
  {
    key: '2a',
    src: './img/slider/slide__02.jpg',
    alt: 'Coffe Picture 2',
    figcaption: '1 Description of the picture goes here'
  },
  {
    key: '3a',
    src: './img/slider/slide__03.jpg',
    alt: 'Coffe Picture 3',
    figcaption: '2 Description of the picture goes here'
  },
  {
    key: '4a',
    src: './img/slider/slide__04.jpg',
    alt: 'Coffe Picture 4',
    figcaption: '3 Description of the picture goes here'
  },
  {
    key: '5a',
    src: './img/slider/slide__05.jpg',
    alt: 'Coffe Picture 5',
    figcaption: '4 Description of the picture goes here'
  },
  {
    key: '6a',
    src: './img/slider/slide__06.jpg',
    alt: 'Coffe Picture 6',
    figcaption: '5 Description of the picture goes here'
  }
];
const postData = [
  {
    id: '0',
    title: 'Wonderful Copenhagen 2011',
    date: 'Posted: 23/1-2011',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__01.jpg',
    alt: 'Post picture 1'
  },
  {
    id: '1',
    title: 'Nordic Barista Cup 2011 in Copenhagen',
    date: 'Posted: 14/12-2010',
    less:
      'Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__02.jpg',
    alt: 'Post picture 2'
  },
  {
    id: '2',
    title: '2010 Winners: Sweden',
    date: 'Posted: 08/10-2010',
    less:
      'Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__03.jpg',
    alt: 'Post picture 3'
  },
  {
    id: '3',
    title: '3 Lorem Ipsum Dolores',
    date: 'Posted: 18/09-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__04.jpg',
    alt: 'Post picture 4'
  },
  {
    id: '4',
    title: '4 Lorem Ipsum Dolores',
    date: 'Posted: 27/08-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__05.jpg',
    alt: 'Post picture 5'
  },
  {
    id: '5',
    title: '5 Lorem Ipsum Dolores',
    date: 'Posted: 15/07-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__06.jpg',
    alt: 'Post picture 6'
  },
  {
    id: '6',
    title: '6 Lorem Ipsum Dolores',
    date: 'Posted: 02/07-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__07.jpg',
    alt: 'Post picture 7'
  },
  {
    id: '7',
    title: '7 Lorem Ipsum Dolores',
    date: 'Posted: 30/06-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__08.jpg',
    alt: 'Post picture 8'
  },
  {
    id: '8',
    title: '8 Lorem Ipsum Dolores',
    date: 'Posted: 26/06-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__09.jpg',
    alt: 'Post picture 9'
  },
  {
    id: '9',
    title: '9 Lorem Ipsum Dolores',
    date: 'Posted: 18/05-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__10.jpg',
    alt: 'Post picture 10'
  },
  {
    title: '10 Lorem Ipsum Dolores',
    title: '4 Neville',
    date: 'Posted: 02/05-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__11.jpg',
    alt: 'Post picture 11'
  },
  {
    id: '11',
    title: '11 Lorem Ipsum Dolores',
    date: 'Posted: 28/04-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__12.jpg',
    alt: 'Post picture 12'
  },
  {
    id: '12',
    title: '12 Lorem Ipsum Dolores',
    date: 'Posted: 23/04-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__13.jpg',
    alt: 'Post picture 13'
  },
  {
    id: '13',
    title: '13 Lorem Ipsum Dolores',
    date: 'Posted: 17/04-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__14.jpg',
    alt: 'Post picture 14'
  },
  {
    id: '14',
    title: '14 Lorem Ipsum Dolores',
    date: 'Posted: 03/03-2010',
    less:
      'The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory',
    more: 'sensory tests, and a lot of educational cuppings.',
    src: './img/articles/square__15.jpg',
    alt: 'Post picture 15'
  }
];

const sponsorsData = [
  {
    alt: 'sponsor',
    src: './img/sponsors/sponsor__01.png'
  },
  {
    alt: 'sponsor2',
    src: './img/sponsors/sponsor__02.png'
  },
  {
    alt: 'sponsor3',
    src: './img/sponsors/sponsor__03.png'
  },
  {
    alt: 'sponsor4',
    src: './img/sponsors/sponsor__04.png'
  },
  {
    alt: 'sponsor5',
    src: './img/sponsors/sponsor__05.png'
  },
  {
    alt: 'sponsor6',
    src: './img/sponsors/sponsor__06.png'
  },
  {
    alt: 'sponsor7',
    src: './img/sponsors/sponsor__07.png'
  },
  {
    alt: 'sponsor8',
    src: './img/sponsors/sponsor__08.png'
  },
  {
    alt: 'sponsor9',
    src: './img/sponsors/sponsor__09.png'
  },
  {
    alt: 'sponsor10',
    src: './img/sponsors/sponsor__10.png'
  },
  {
    alt: 'sponsor11',
    src: './img/sponsors/sponsor__11.png'
  },
  {
    alt: 'sponsor12',
    src: './img/sponsors/sponsor__12.png'
  },
  {
    alt: 'sponsor13',
    src: './img/sponsors/sponsor__13.png'
  },
  {
    alt: 'sponsor14',
    src: './img/sponsors/sponsor__14.png'
  },
  {
    alt: 'sponsor15',
    src: './img/sponsors/sponsor__15.png'
  },
  {
    alt: 'sponsor16',
    src: './img/sponsors/sponsor__16.png'
  },
  {
    alt: 'sponsor17',
    src: './img/sponsors/sponsor__17.png'
  },
  {
    alt: 'sponsor18',
    src: './img/sponsors/sponsor__18.png'
  },
  {
    alt: 'sponsor19',
    src: './img/sponsors/sponsor__19.png'
  },
  {
    alt: 'sponsor20',
    src: './img/sponsors/sponsor__20.png'
  },
  {
    alt: 'sponsor21',
    src: './img/sponsors/sponsor__21.png'
  },
  {
    alt: 'sponsor22',
    src: './img/sponsors/sponsor__22.png'
  }
];
