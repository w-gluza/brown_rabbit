let images = dataSlides.map(image => image.src);
let posts = postData.map(
  post =>
    `<figure>
      <img class="post__img" src="${post.src}" alt="${post.alt}"  />
     </figure>
     <div>
       <h3>${post.title}</h3>
       <p class="post__subheading">${post.date}</p>
       <p>${post.less}<span id="ellipsis${post.id}">...</span>
       <span class="seeMore" id="more${post.id}">${post.more}.</span>
       </p>
       <button class="button" id=${post.id}>Read more</button>
     </div>`
);
let sponsors = sponsorsData.map(
  sponsor =>
    `<img class="img__sponsor" src="${sponsor.src}" alt="${sponsor.alt}" />`
);

document.addEventListener('click', function(event) {
  if (!event.target.matches('.button')) {
    return;
  }
  const id = event.target.id;
  const ellipsis = document.querySelector('#ellipsis' + id);
  const toggleButton = document.getElementById(id);
  const seeMore = document.querySelector('#more' + id);
  if (ellipsis.style.display === 'none') {
    ellipsis.style.display = 'inline';
    toggleButton.innerHTML = 'Read more';
    seeMore.style.display = 'none';
  } else {
    ellipsis.style.display = 'none';
    toggleButton.innerHTML = 'Read less';
    seeMore.style.display = 'inline';
  }
});

let slideIndex = 0;
let sliceStart = 0;
let sliceEnd = 3;
let sliderLength;
const slide = document.querySelector('#slide');
const postContainer = document.querySelector('#postContainer');
const sponsorContainer = document.querySelector('#sponsorContainer');

sliderLength = images.length;
slideIndex = Math.floor(Math.random() * sliderLength);
slide.src = images[slideIndex];

postContainer.innerHTML = posts.slice(sliceStart, sliceEnd).join('');
sponsorContainer.innerHTML = sponsors.join('');

// carusel
document.querySelector('#nextImage').addEventListener('click', function() {
  if (slideIndex < images.length - 1) {
    slideIndex++;
  } else {
    slideIndex = 0;
  }
  slide.src = images[slideIndex];
});

document.querySelector('#previousImage').addEventListener('click', function() {
  if (slideIndex <= 0) {
    // Add 1 to Index
    slideIndex = images.length - 1;
  } else if (slideIndex <= images.length - 1) {
    slideIndex--;
  }
  slide.src = images[slideIndex];
});

// pagination
document.querySelector('#nextPage').addEventListener('click', function() {
  if (sliceEnd < posts.length) {
    sliceStart = sliceStart + 3;
    sliceEnd = sliceEnd + 3;
  }
  postContainer.innerHTML = posts.slice(sliceStart, sliceEnd).join('');
});

document.querySelector('#previousPage').addEventListener('click', function() {
  if (sliceEnd <= 3) {
    sliceStart = 0;
    sliceEnd = 3;
  } else if (sliceEnd <= posts.length) {
    sliceStart = sliceStart - 3;
    sliceEnd = sliceEnd - 3;
  }
  postContainer.innerHTML = posts.slice(sliceStart, sliceEnd).join('');
});

// search function
document.querySelector('#submit__search').addEventListener('click', function() {
  const input = document.getElementById('findInput').value;
  if (input === '') {
    alert('It is pretty hard to find something without the input :<');
    return;
  }
  if (window.find) {
    let found = window.find(input);
    if (!found) {
      alert('We could not find: \n' + input);
    }
  }
});

// burger handler
document.getElementById('nav__btn').addEventListener('click', function() {
  document.getElementById('nav__items').classList.toggle('open');
});
